<?php
use \yii\redactor\widgets\Redactor;
use \yii\helpers\Html;
use \yii\widgets\ActiveForm; 
?>

<?php $form = ActiveForm::begin(); ?> 

<?= $form->errorSummary($model) ?>

<?= $form->field($model, 'active')->checkbox() ?>

<?= $form->field($model, 'name') ?>

<?= $form->field($model, 'order') ?>

<?= $form->field($model, 'icon') ?>

<?= $form->field($model, 'markdown')->textArea(['rows' => 15]) ?>

<?= $form->field($model, 'html')->widget(Redactor::className(), ['clientOptions' => [
	'minHeight' => 200,
]]) ?>

<div class="form-group">
	<?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?> 
</div>