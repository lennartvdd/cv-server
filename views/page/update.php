<?php
$this->params['breadcrumbs'] = [
	['label' => 'Pages',  'url' => ['page/index']],
	['label' => 'View: ' . $model->name, 'url' => ['page/view', 'id' => (string)$model->_id]],
	['label' => 'Update'],
];
?>
<h1>Update Page <?= $model->name ?></h1>

<?= $this->render('_form', ['model' => $model]) ?>