<?php
use yii\helpers\Html;

$this->params['breadcrumbs'] = [
	['label' => 'Pages',  'url' => ['page/index']],
	['label' => 'View: ' . $model->name],
];
?>

<h1>View Page: <?= $model->name ?> <?= !$model->active ? "(inactive)" : "" ?></h1>

<?= Html::a('Update', ['update', 'id' => (string)$model->_id], ['class' => 'btn btn-primary']) ?> 
<?= Html::a('Create', ['create', 'id' => (string)$model->_id], ['class' => 'btn btn-default']) ?>

<br />
<br />
<strong>Icon:</strong> <?= $model->icon ?><br />

<h2>HTML</h2>
<?= $model->html ?>

<h2>Markdown</h2>
<pre>
<?= $model->markdown ?>
</pre>