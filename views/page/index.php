<?php
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\data\ActiveDataProvider;
use app\models\Page;
use yii\helpers\Html;

$this->params['breadcrumbs'] = [
    ['label' => 'Pages'],
];

?>

<h1>Page Overview</h1>

<?= Html::a('Create', ['create'], ['class' => 'btn btn-primary']) ?>
<br /><br />
<?php

$dataProvider = new ActiveDataProvider([
    'query' => Page::find(),
    'pagination' => [
        'pageSize' => 20,
    ],
]);
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
    	'name',
        'order',
    	[
    		'class' => ActionColumn::className(),
    	],
    ],
]);
