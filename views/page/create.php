<?php
$this->params['breadcrumbs'] = [
	['label' => 'Pages',  'url' => ['page/index']],
	['label' => 'Create'],
];
?>
<h1>Create new Page</h1>

<?= $this->render('_form', ['model' => $model]) ?>