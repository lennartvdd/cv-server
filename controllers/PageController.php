<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\Page;
use yii\web\NotFoundHttpException;

class PageController extends Controller 
{

	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['logout'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => false,
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

	public function actionIndex()
	{
		$model = new Page();
		return $this->render('index', [
			'model' => $model,
		]);
	}	

	public function actionUpdate($id) 
	{
		$model = $this->loadModel($id);
		
		$data = Yii::$app->request->post();
		if($model->load($data) && $model->save()) {
			$this->redirect(['view', 'id' => (string)$model->_id]);
		}

		return $this->render('update', ['model' => $model]);
	}

	public function actionCreate()
	{
		$model = new Page();

		$data = Yii::$app->request->post();
		if($model->load($data) && $model->save()) {
			$this->redirect(['view', 'id' => (string)$model->_id]);
		}

		return $this->render('create', ['model' => $model]);
	}

	public function actionView($id)
	{
		$model = $this->loadModel($id);
		return $this->render('view', [
			'model' => $model,
		]);
	}

	public function actionDelete($id)
	{
		if($this->loadModel($id)->delete()) {
			$this->redirect(['index']);
		}
	}

	private function loadModel($id)
	{
		$model = Page::findOne($id);
		if(!$model) {
			throw new NotFoundHttpException();
		}
		return $model;
	}
}
