<?php

namespace app\modules\api\models;

use app\models\Page as PageModel;

class Page extends PageModel 
{
	public function fields()
	{
		return [
			'id' => function($model){ return (string)$model->_id; },
			'name',
			'html',
			'icon',
			'markdown',
		];
	}

	public static function find()
	{
		return parent::find()->where(['active' => true]);
	}
}