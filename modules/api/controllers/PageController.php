<?php
namespace app\modules\api\controllers;

use Yii;
use yii\rest\ActiveController;
use app\modules\api\models\Page;

use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\HttpBasicAuth;

class PageController extends ActiveController 
{
	public $modelClass = 'app\modules\api\models\Page';

	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => CompositeAuth::className(),
			'authMethods' => [
				HttpBasicAuth::className(),
				HttpBearerAuth::className(),
			],
		];
		return $behaviors;
	}
}
