<?php

namespace app\models;

use yii\mongodb\ActiveRecord;

class Page extends ActiveRecord 
{
	
	public static function collectionName()
	{
		return 'pages';
	}

	public function attributes()
	{
		return ['_id', 'name', 'html', 'markdown', 'icon', 'order', 'active'];
	}

	public function rules()
	{
		return [
			[['name', 'icon'], 'string'],
			[['html', 'markdown'], 'string'],
			[['order'], 'integer'],
			['active', 'boolean'],
		];
	}

	public function beforeSave($insert)
	{
		$this->typeCastAttributes();
		return parent::beforeSave($insert);
	}

	public function afterFind()
	{
		$this->typeCastAttributes();
		return parent::afterFind();
	}

	public function typeCastAttributes()
	{
		$this->order = (int)$this->order;
		$this->active = (bool)$this->active;
	}

	public static function find($condition = '', $params = [])
	{
		return parent::find($condition, $params)->orderBy('order ASC, name ASC');
	}

}